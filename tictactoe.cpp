#include <iostream>

char board[9] = {};
char const PLAYER_X = 'X';
char const PLAYER_O = 'O';
char const STATUS_DRAW = 'D';
int const CLEAR_ROWS_NUM = 30;

void clearScreen() {
    for (int i = 0; i < CLEAR_ROWS_NUM; ++i) {
        std::cout << std::endl;
    }
}

void clearBoard() {
    for (char &i : board) {
        i = '-';
    }
}

bool hasWon(char player) {
    int wins[][3] = {{0, 1, 2},
                     {3, 4, 5},
                     {6, 7, 8},
                     {0, 3, 6},
                     {1, 4, 7},
                     {2, 5, 8},
                     {0, 4, 8},
                     {2, 4, 6}};

    for (auto &win : wins) {
        int count = 0;
        for (int j : win) {
            if (board[j] == player) {
                count++;
            }
        }
        if (3 == count) {
            return true;
        }
    }

    return false;
}

void printBoard(const std::string &indent) {
    std::cout << std::endl;
    std::cout << indent << "-" << board[6] << "-|-" << board[7] << "-|-" << board[8] << "-\n";
    std::cout << indent << "-" << board[3] << "-|-" << board[4] << "-|-" << board[5] << "-\n";
    std::cout << indent << "-" << board[0] << "-|-" << board[1] << "-|-" << board[2] << "-\n";
}

int getTurn() {
    std::cout << "\nMove options: \n";
    std::cout << "-7-|-8-|-9-" << std::endl;
    std::cout << "-4-|-5-|-6-" << std::endl;
    std::cout << "-1-|-2-|-3-" << std::endl;
    std::cout << "\n";

    printBoard("");
    std::cout << "\nYour move: ";

    int move;
    std::cin >> move;
    while (move > 9 || move < 1 || board[move - 1] != '-') {
        std::cout << "Please enter only digits (1-9): \n";
        std::cin >> move;
    }

    return move;
}

char playAndGetWinner() {
    int turn = 1;

    while (!hasWon(PLAYER_X) && !hasWon(PLAYER_O)) {
        clearScreen();
        int move = getTurn();
        clearScreen();
        if (1 == turn % 2) {
            board[move - 1] = PLAYER_X;
            if (hasWon(PLAYER_X)) {
                std::cout << "Congratulations player X! You have won!\n";
                return PLAYER_X;
            }
        } else {
            board[move - 1] = PLAYER_O;
            if (hasWon(PLAYER_O)) {
                std::cout << "Congratulations player O! You have won!\n";
                return PLAYER_O;
            }
        }
        turn++;
        if (10 == turn) {
            std::cout << "\t It's a draw!\n";
            return 'D';
        }
    }
}

int main() {
    std::cout << "Welcome to tic-tac-toe game!\n\n";
    std::string reply = "y";
    int xWins = 0, oWins = 0, ties = 0;

    while ("y" == reply) {
        clearBoard();
        char winner = playAndGetWinner();
        printBoard("\t    ");

        switch (winner) {
            case PLAYER_X:
                xWins++;
                break;
            case PLAYER_O:
                oWins++;
                break;
            case STATUS_DRAW:
                ties++;
                break;
            default:
                std::cout << "\nSomething went wrong\n";
                break;
        }

        std::cout << "\n\t*Winner Statistics*\nPlayer X: " << xWins
                  << ", Player O: " << oWins << " and Ties: " << ties << "\n\n";
        std::cout << "\nWould you like to play again? (y/n): ";
        std::cin >> reply;
        while ("y" != reply && "n" != reply) {
            std::cout << "Please enter a valid reply (y/n):";
            std::cin >> reply;
        }
    }

    return 0;
}